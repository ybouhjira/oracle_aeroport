
DROP VIEW Place_view ;
DROP VIEW Vol_view ;
--------------------------------------------------------------------------------
-- CONTRAINTES DE CLE ETRANGERES
--------------------------------------------------------------------------------

-- Table Place
---------------
ALTER TABLE Place
DROP CONSTRAINT place_num_vol_fk;

ALTER TABLE Place
DROP CONSTRAINT place_num_classe_fk;

ALTER TABLE Place
DROP CONSTRAINT place_num_type_place_fk;

-- Table Bagage
----------------
ALTER TABLE Bagage
DROP CONSTRAINT bagage_cin_fk;

-- Table Vol
-------------

ALTER TABLE Vol
DROP CONSTRAINT vol_num_compagnie_fk;

ALTER TABLE Vol
DROP CONSTRAINT vol_num_aeroport_fk;

---------------------------------------------------------------------
-- TABLES
----------------------------------------------------------------------
DROP TABLE Client;
DROP TABLE Place;
DROP TABLE Classe;
DROP TABLE TypePlace ;
DROP TABLE Bagage ;
DROP TABLE Vol;
DROP TABLE Compagnie;
DROP TABLE Aeroport;

