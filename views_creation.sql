--place view

CREATE OR REPLACE VIEW Place_view 
AS 
    SELECT
        p.num_place, 
        c.cin ,
        cla.nom_classe AS classe,
        p.prix AS prix,
        ty.nom_type_place AS type,
        p.num_vol
    FROM Client c ,Classe cla, Place p,TypePlace ty
    WHERE 
        p.cin = c.cin 
        AND p.num_classe = cla.num_classe 
        AND p.num_type_place = ty.num_type_place ;
    
    
-- vol view
CREATE OR REPLACE VIEW Vol_view 
AS
    SELECT v.num_vol,
    v.date_depart ,
    v.date_arrive,
    co.nom_compagnie AS Compagnie,
    ae.pays_aeroport||', '||ae.ville_aeroport AS Aeroport 
    FROM Vol v ,Compagnie co, Aeroport ae 
    WHERE co.num_compagnie = v.num_compagnie
        AND ae.num_aeroport = v.num_aeroport;     
