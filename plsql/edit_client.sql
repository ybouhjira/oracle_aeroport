SET SERVEROUTPUT ON
SET VERIFY OFF
ACCEPT s_cin PROMPT 'Entrez le CIN du client: '
PROMPT Entrez les nouvelles valeurs

ACCEPT s_nom PROMPT 'Nom: '
ACCEPT s_prenom PROMPT 'Prenom: '
ACCEPT s_tel PROMPT 'Telephone: '
ACCEPT s_email PROMPT 'E-mail: '
ACCEPT s_mot_de_passe PROMPT 'Mot de passe: '
ACCEPT s_adresse PROMPT 'Adresse: '
ACCEPT s_ville PROMPT 'Ville: '
ACCEPT s_pays PROMPT 'Pays: '

DECLARE
	e_no_client EXCEPTION;
BEGIN
	UPDATE Client
	SET
		nom = '&s_nom',
		prenom = '&s_prenom',
		tel = '&s_tel',
		email = '&s_email',
		mot_de_passe = '&s_mot_de_passe',
		adresse = '&s_adresse',
		ville = '&s_ville',
		pays = '&s_pays'
	WHERE cin = '&s_cin';
	
	IF SQL%NOTFOUND THEN
		RAISE e_no_client;
	END IF;

EXCEPTION
	WHEN e_no_client THEN
		DBMS_OUTPUT.PUT_LINE('Client inexistant');
	WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Une erreur est survenue : ');
        DBMS_OUTPUT.PUT_LINE('CODE: '||TO_CHAR(SQLCODE));
        DBMS_OUTPUT.PUT_LINE('MESSAGE: '||SQLERRM);
END ;
/

