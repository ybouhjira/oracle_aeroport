SET SERVEROUTPUT ON
SET VERIFY OFF
ACCEPT s_cin PROMPT 'CIN : '
ACCEPT s_nom PROMPT 'Nom : '
ACCEPT s_prenom PROMPT 'Prenom : '
ACCEPT s_tel PROMPT 'Telephone : '
ACCEPT s_email PROMPT 'E-mail : '
ACCEPT s_mot_de_passe PROMPT 'Mot de passe : '
ACCEPT s_adresse PROMPT 'Adresse : '
ACCEPT s_ville PROMPT 'Ville : '
ACCEPT s_pays PROMPT 'Pays : '

BEGIN
	INSERT INTO Client(
		cin,
		nom,
		prenom, 
		tel,
		email,
		mot_de_passe,
		adresse,
		ville,
		pays
    )
    VALUES(
    	'&s_cin',
		'&s_nom',
		'&s_prenom', 
		'&s_tel',
		'&s_email',
		'&s_mot_de_passe',
		'&s_adresse',
		'&s_ville',
		'&s_pays'
	);
	DBMS_OUTPUT.PUT_LINE('Client ajoute avec succes.');
EXCEPTION
	WHEN DUP_VAL_ON_INDEX THEN
		DBMS_OUTPUT.PUT_LINE('Un client avec le cin : &s_cin existe deja.');
	WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Une erreur est survenue : ');
        DBMS_OUTPUT.PUT_LINE('CODE: '||TO_CHAR(SQLCODE));
        DBMS_OUTPUT.PUT_LINE('MESSAGE: '||SQLERRM);
END ;
/

