SET SERVEROUTPUT ON
SET VERIFY OFF 
ACCEPT s_cin_client PROMPT 'Entrer votre CIN SVP :'

-- COMPTER LE NOMBRE DE BAGAGES ET PLACES
DECLARE
   v_count NUMBER(2);
   e_no_client EXCEPTION;
BEGIN
    -- S'ASSURER QUE LE CLIENT EXISTE
    SELECT COUNT(cin)
    INTO v_count
    FROM client
    WHERE cin = '&s_cin_client';
    IF v_count = 0 THEN
        RAISE e_no_client ;
    END IF;

    -- SUPRESSION
    -- Supprimer les bagages :
    SELECT COUNT(num_bagage)
    INTO v_count
    FROM Bagage
    WHERE cin = '&s_cin_client' ;

    DBMS_OUTPUT.PUT_LINE('-- Supression de '||TO_CHAR(v_count)||' bagages.');
    
    DELETE FROM Bagage 
    WHERE cin = '&s_cin_client' ;

    -- Supprimer les places :
    SELECT COUNT(num_place)
    INTO v_count
    FROM Place_view
    WHERE cin = '&s_cin_client' ;

    DBMS_OUTPUT.PUT_LINE('-- Supression de '||TO_CHAR(v_count)||' places.');
    
    DELETE FROM Place 
    WHERE cin = '&s_cin_client' ;

    -- Supprimer le clients :
    DBMS_OUTPUT.PUT_LINE('-- Supression du client.');
    DELETE FROM Client 
    WHERE cin = '&s_cin_client' ;

EXCEPTION
    WHEN e_no_client THEN
        DBMS_OUTPUT.PUT_LINE('Client inexistant');
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Une erreur est survenue : ');
        DBMS_OUTPUT.PUT_LINE('CODE: '||TO_CHAR(SQLCODE));
        DBMS_OUTPUT.PUT_LINE('MESSAGE: '||SQLERRM);
END ;
/