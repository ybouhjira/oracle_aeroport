SET SERVEROUTPUT ON
ACCEPT s_cin_client PROMPT 'Entrer le CIN de client : '
SET VERIFY OFF
DECLARE
   v_count NUMBER(2);
   e_no_client EXCEPTION;
BEGIN
    -- S'ASSURER QUE LE CLIENT EXISTE
    SELECT COUNT(cin)
    INTO v_count
    FROM client
    WHERE cin = '&s_cin_client';
    IF v_count = 0 THEN
        RAISE e_no_client ;
    END IF;

    -- AFFICHER LES INFORMATIONS DU CLIENT
    DBMS_OUTPUT.PUT_LINE('================================================================================');
    DBMS_OUTPUT.PUT_LINE('-------------------------------- CLIENT ----------------------------------------');
    DBMS_OUTPUT.PUT_LINE('================================================================================');
    FOR i IN (
        SELECT nom, prenom, email, tel, adresse, mot_de_passe, ville, pays 
        FROM Client 
        WHERE cin = '&s_cin_client' 
        )      
        LOOP
            DBMS_OUTPUT.PUT_LINE('Nom : ' ||i.nom);
            DBMS_OUTPUT.PUT_LINE('Prenom : ' ||i.prenom);
            DBMS_OUTPUT.PUT_LINE('Email : ' ||i.email);
            DBMS_OUTPUT.PUT_LINE('Tel : ' ||i.tel);
            DBMS_OUTPUT.PUT_LINE('Adresse : ' ||i.adresse);
            DBMS_OUTPUT.PUT_LINE('Mot de passe : ' ||i.mot_de_passe);
            DBMS_OUTPUT.PUT_LINE('Ville : ' ||i.ville);
            DBMS_OUTPUT.PUT_LINE('Pays : ' ||i.pays);
    END LOOP;

    -- AFFICHER LES PLACES RESERVES PAR LE CLIENT
    DBMS_OUTPUT.PUT_LINE('================================================================================');
    DBMS_OUTPUT.PUT_LINE('-------------------------------- PLACES ----------------------------------------');

        -- Compter le nombre de places :
    SELECT COUNT(num_place)
    INTO v_count
    FROM Place_view
    WHERE cin = '&s_cin_client' ;
    DBMS_OUTPUT.PUT_LINE('Nombre de places : '||TO_CHAR(v_count));
    DBMS_OUTPUT.PUT_LINE('--------------------------------------------------------------------------------');

        -- Afficher les places :
    FOR i IN (
        SELECT num_place, classe, prix, type, num_vol 
        FROM Place_view 
        WHERE cin = '&s_cin_client' 
        )      
        LOOP
            DBMS_OUTPUT.PUT_LINE('Numero : ' ||TO_CHAR(i.num_place));
            DBMS_OUTPUT.PUT_LINE('Classe : ' ||i.classe);
            DBMS_OUTPUT.PUT_LINE('Prix : ' ||TO_CHAR(i.prix));
            DBMS_OUTPUT.PUT_LINE('Type : ' ||i.type);
            DBMS_OUTPUT.PUT_LINE('Vol : ' ||TO_CHAR(i.num_vol));
            DBMS_OUTPUT.PUT_LINE('--------------------------------------------------------------------------------');
    END LOOP;

    -- AFFICHER LES BAGAGES DU CLIENT
    DBMS_OUTPUT.PUT_LINE('================================================================================');
    DBMS_OUTPUT.PUT_LINE('-------------------------------- BAGAGES ---------------------------------------');

    -- Compter le nombre de bagages :
    SELECT COUNT(num_bagage)
    INTO v_count
    FROM Bagage
    WHERE cin = '&s_cin_client' ;
    DBMS_OUTPUT.PUT_LINE('Nombre de bagages : '||TO_CHAR(v_count));
    DBMS_OUTPUT.PUT_LINE('--------------------------------------------------------------------------------');

    -- Afficher les bagages :
    FOR i IN (
        SELECT num_bagage, poids, designation 
        FROM Bagage 
        WHERE cin = '&s_cin_client' 
        )      
        LOOP
            DBMS_OUTPUT.PUT_LINE('Numero : ' ||TO_CHAR(i.num_bagage));
            DBMS_OUTPUT.PUT_LINE('Classe : ' ||TO_CHAR(i.poids));
            DBMS_OUTPUT.PUT_LINE('Prix : ' ||i.designation);
            DBMS_OUTPUT.PUT_LINE('--------------------------------------------------------------------------------');
    END LOOP;
EXCEPTION
    WHEN e_no_client THEN
        DBMS_OUTPUT.PUT_LINE('Client inexistant');
    WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Une erreur est survenue : ');
        DBMS_OUTPUT.PUT_LINE('CODE: '||TO_CHAR(SQLCODE));
        DBMS_OUTPUT.PUT_LINE('MESSAGE: '||SQLERRM);
END ;
/