
ACCEPT s_cin_client PROMPT 'Entrer le CIN de client : '
DECLARE 
    CURSOR cur_place IS
        SELECT num_place,num_vol,prix FROM Place;    
    CURSOR cur_client IS
        SELECT nom,prenom,email,tel,adresse,mot_de_passe,ville,pays FROM Client ;
    CURSOR cur_bagage IS
        SELECT  poids,designation,num_bagage FROM Bagage ;
BEGIN 
    OPEN cur_client;
        FOR cur_client IN
        (SELECT nom,prenom,email,tel,adress e,mot_de_passe,ville,pays FROM Client WHERE cin=&s_cin_client )
        LOOP
            DBMS_OUTPUT.PUT_LINE('Nom :' ||cur_client.nom);
            DBMS_OUTPUT.PUT_LINE('Prenom :' ||cur_client.prenom);
            DBMS_OUTPUT.PUT_LINE('Email :' ||cur_client.email);
            DBMS_OUTPUT.PUT_LINE('Tel :' ||cur_client.tel);
            DBMS_OUTPUT.PUT_LINE('Adresse :' ||cur_client.adresse);
            DBMS_OUTPUT.PUT_LINE('Mot de passe :' ||cur_client.mot_de_passe);
            DBMS_OUTPUT.PUT_LINE('Ville :' ||cur_client.ville);
            DBMS_OUTPUT.PUT_LINE('Pays :' ||cur_client.pays);
        END LOOP;
     CLOSE cur_client;   
     DBMS_OUTPUT.PUT_LINE('====================================================================================');
   --Sum of weight
    SELECT SUM(poids) INTO 
    v_sum_poids 
    FROM bagage 
    WHERE cin = &s_cin_client;
    DBMS_OUTPUT.PUT_LINE('Somme de poids : ' ||v_sum_poids );
    OPEN cur_bagage;
        FOR cur_bagage IN
         (SELECT poids,designation,num_bagage FROM bagage WHERE cin = &s_cin_client) 
         LOOP
            DBMS_OUTPUT.PUT_LINE('Poids :'||cur_bagage.poids ||'Kg      ');
            DBMS_OUTPUT.PUT_LINE('designation : '||cur_bagage.designation);
            DBMS_OUTPUT.PUT_LINE('Numéro de bagage : '||cur_bagage.num_bagage);
            DBMS_OUTPUT.PUT_LINE('----------------------------------------------------------------------------');
         END LOOP;            
    CLOSE cur_bagage;
    DBMS_OUTPUT.PUT_LINE('====================================================================================');
    OPEN cur_place
        FOR cur_place IN
        (SELECT num_place,num_vol,prix FROM Place WHERE cin = &s_cin_client)
    LOOP
         DBMS_OUTPUT.PUT_LINE('Numéro de place  :'||cur_place.num_place );
         DBMS_OUTPUT.PUT_LINE('Numéro de vol  :'||cur_place.num_vol );
         DBMS_OUTPUT.PUT_LINE('Prix  :'||cur_place.prix ); 
    END LOOP;
    CLOSE cur_place;
    DBMS_OUTPUT.PUT_LINE('====================================================================================');
END ;
/



