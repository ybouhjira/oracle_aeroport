CREATE TABLE Client(
    cin VARCHAR2(10) PRIMARY KEY,
    nom VARCHAR2(45) NOT NULL,
    prenom VARCHAR2(45) NOT NULL,
    tel VARCHAR2(15) NOT NULL,
    email VARCHAR2(256) NOT NULL,
    mot_de_passe VARCHAR2(30) NOT NULL,
    adresse VARCHAR2(500) NOT NULL,
    ville VARCHAR2(45) NOT NULL,
    pays VARCHAR2(45) NOT NULL
);

CREATE TABLE Place(
    num_place NUMBER(3),
    num_vol NUMBER(3),
    cin VARCHAR2(10) NOT NULL,
    num_classe NUMBER(1) NOT NULL,
    num_type_place NUMBER(1) NOT NULL,
    prix NUMBER(7,2) NOT NULL,
    CONSTRAINT place_num_plc_vol_pk PRIMARY KEY (num_place , num_vol)
);

CREATE TABLE Classe (
    num_classe NUMBER(1) PRIMARY KEY,
    nom_classe VARCHAR2(20) NOT NULL
);

CREATE TABLE TypePlace (
    num_type_place NUMBER(1) PRIMARY KEY,
    nom_type_place VARCHAR2(6) NOT NULL
);

CREATE TABLE Bagage(
    num_bagage NUMBER(6) PRIMARY KEY,
    cin VARCHAR2(10) NOT NULL,
    poids NUMBER(5,2) NOT NULL,
    designation VARCHAR2(500)
);

CREATE TABLE Vol(
    num_vol NUMBER(3) PRIMARY KEY,
    date_depart DATE NOT NULL,
    date_arrive DATE NOT NULL,
    num_compagnie NUMBER(2) NOT NULL,
    num_aeroport NUMBER(3) NOT NULL
);

CREATE TABLE Compagnie(
    num_compagnie NUMBER(2) PRIMARY KEY,
    nom_compagnie VARCHAR2(45) NOT NULL,
    email_compagnie VARCHAR2(256) NOT NULL,
    tel_compagnie VARCHAR2(15) NOT NULL
);

CREATE TABLE Aeroport(
    num_aeroport NUMBER(3) PRIMARY KEY,
    ville_aeroport VARCHAR2(45) NOT NULL,
    pays_aeroport VARCHAR2(45) NOT NULL
);

--------------------------------------------------------------------------------
-- CONTRAINTES DE CLE ETRANGERES
--------------------------------------------------------------------------------

-- Table Place
---------------
ALTER TABLE Place
ADD CONSTRAINT place_num_vol_fk
FOREIGN KEY (num_vol)
REFERENCES Vol(num_vol);

ALTER TABLE Place
ADD CONSTRAINT place_num_classe_fk
FOREIGN KEY (num_classe)
REFERENCES Classe(num_classe);

ALTER TABLE Place
ADD CONSTRAINT place_num_type_place_fk
FOREIGN KEY (num_type_place)
REFERENCES TypePlace(num_type_place);

-- Table Bagage
----------------
ALTER TABLE Bagage
ADD CONSTRAINT bagage_cin_fk
FOREIGN KEY (cin)
REFERENCES Client(cin);

-- Table Vol
-------------

ALTER TABLE Vol
ADD CONSTRAINT vol_num_compagnie_fk
FOREIGN KEY (num_compagnie)
REFERENCES Compagnie(num_compagnie);

ALTER TABLE Vol
ADD CONSTRAINT vol_num_aeroport_fk
FOREIGN KEY (num_aeroport)
REFERENCES Aeroport(num_aeroport);