#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlDatabase>
class QTabWidget;
class QTableView;
class QSqlTableModel;
class QSqlRelationalTableModel;


class MainWindow : public QMainWindow
{
    Q_OBJECT
// METHODS
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void showAboutDialog();
    void showConnectionDialog();

// ATTRIBUTES
private:
    QMenuBar *m_menubar ;
    QMenu
        *m_helpMenu,
        *m_fileMenu,
        *m_optionsMenu;
    QAction
        *m_quitAction,
        *m_dbConAction,
        *m_aboutAction;
    QTabWidget *m_tabWidget;
    QTableView
        *m_clientView,
        *m_bagageView,
        *m_placeView,
        *m_aeroportView,
        *m_volView,
        *m_compagnieView;
    QSqlDatabase m_database;
    QSqlRelationalTableModel *m_placeModel;
    QSqlTableModel
        *m_clientModel,
        *m_bagageModel,
        *m_aeroportModel,
        *m_volModel,
        *m_compagnieModel;
};

#endif // MAINWINDOW_H
