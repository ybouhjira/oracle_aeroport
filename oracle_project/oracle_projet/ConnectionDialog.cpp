﻿#include "ConnectionDialog.h"
#include <QPushButton>
#include <QLineEdit>
#include <QFormLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFile>
#include <QMessageBox>
#include <QTextStream>

ConnectionDialog::ConnectionDialog(QWidget *parent) :
    QDialog(parent)
  , m_okButton(new QPushButton("Valider"))
  , m_cancelButton(new QPushButton("Annuler"))
  , m_passLineEdit(new QLineEdit)
  , m_userLineEdit(new QLineEdit)
  , m_dbLineEdit(new QLineEdit)
  , m_hostLineEdit(new QLineEdit)
  , m_mainLayout(new QVBoxLayout(this))
  , m_formLayout(new QFormLayout)
  , m_btnLayout(new QHBoxLayout)
{

    // LAYOUT ------------------------------------------------------------------
    m_mainLayout->addLayout(m_formLayout);
    m_mainLayout->addLayout(m_btnLayout);

    // form layout
    m_formLayout->addRow("Host : ",m_hostLineEdit);
    m_formLayout->addRow("Base de donnees : ",m_dbLineEdit);
    m_formLayout->addRow("Utilisateur : ",m_userLineEdit);
    m_formLayout->addRow("Mot de passe : ", m_passLineEdit);

    // Buttons layout
    m_btnLayout->addWidget(m_okButton);
    m_btnLayout->addWidget(m_cancelButton);

    // CONNECTIONS -------------------------------------------------------------
    connect(m_okButton, SIGNAL(clicked()), this , SLOT(accept()) );
    connect(m_cancelButton, SIGNAL(clicked()), this, SLOT(reject()) );
}

void ConnectionDialog::getOptions(QWidget *parent, const QString &title)
{
    ConnectionDialog dialog(parent);
    dialog.setWindowTitle(title);
    dialog.readOptions();
    if(dialog.exec() == QDialog::Accepted){
        dialog.writeOptions();
    }else{

    }
}

void ConnectionDialog::readOptions(){
    QFile optFile("connection.config");
    if(optFile.open(QIODevice::ReadOnly)){
        m_hostLineEdit->setText(QString(optFile.readLine()).trimmed());
        m_dbLineEdit->setText(QString(optFile.readLine()).trimmed());
        m_userLineEdit->setText(QString(optFile.readLine()).trimmed());
        m_passLineEdit->setText(QString(optFile.readLine()).trimmed());
    }else{
        QString msg("Impossible d'ouvrir le fichier de configuration");
        QMessageBox::warning(this,"Erreur",msg);
    }
}

void ConnectionDialog::writeOptions(){
    QFile optFile("connection.config");
    if(optFile.open(QIODevice::WriteOnly)){
        QTextStream out(&optFile);
        out << m_hostLineEdit->text().trimmed() << '\n' ;
        out << m_dbLineEdit->text().trimmed() << '\n' ;
        out << m_userLineEdit->text().trimmed() << '\n' ;
        out << m_passLineEdit->text().trimmed() << '\n' ;
    }else{
        QString msg("Impossible d'ouvrir le fichier de configuration");
        QMessageBox::warning(this,"Erreur",msg);
    }
}
