#-------------------------------------------------
#
# Project created by QtCreator 2012-12-15T17:08:36
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = oracle_projet
TEMPLATE = app


SOURCES += main.cpp\
        MainWindow.cpp \
    ConnectionDialog.cpp

HEADERS  += MainWindow.h \
    ConnectionDialog.h

OTHER_FILES += \
    connection.config
