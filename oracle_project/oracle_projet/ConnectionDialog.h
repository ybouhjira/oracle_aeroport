#ifndef ORACLECONNECTIONDIALOG_H
#define ORACLECONNECTIONDIALOG_H

#include <QDialog>
class QPushButton;
class QLineEdit;
class QFormLayout;
class QVBoxLayout;
class QHBoxLayout;
class QTableView;

class ConnectionDialog : public QDialog
{
    Q_OBJECT
public:
    static void getOptions(QWidget *parent, QString const& title);
private:
    explicit ConnectionDialog(QWidget *parent = 0);

    /**
     * @brief reads the options from connection.config and puts them
     * into the QlineEdits
     */
    void readOptions();

    /**
     * @brief writes the options from the lineEdits
     * to the connection.config file
     **/
    void writeOptions();

// ATTRIBUTES:
private:
    QPushButton
        *m_okButton,
        *m_cancelButton;
    QLineEdit
        *m_passLineEdit,
        *m_userLineEdit,
        *m_dbLineEdit,
        *m_hostLineEdit;
    QVBoxLayout *m_mainLayout;
    QFormLayout *m_formLayout;
    QHBoxLayout *m_btnLayout;
};

#endif // ORACLECONNECTIONDIALOG_H
