#include "MainWindow.h"
#include <QMenuBar>
#include <QMenu>
#include <QMessageBox>
#include <QApplication>
#include <QTableWidget>
#include <QTableView>
#include <QSqlTableModel>
#include <QApplication>
#include <QFile>
#include <QSqlRelationalTableModel>
#include "ConnectionDialog.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_menubar(new QMenuBar)
    , m_helpMenu(new QMenu("Aide"))
    , m_fileMenu(new QMenu("Fichier"))
    , m_optionsMenu(new QMenu("Options"))
    , m_quitAction(new QAction("Quitter",this))
    , m_dbConAction(new QAction("Connction a Oracle",this))
    , m_aboutAction(new QAction("A propos",this))
    , m_tabWidget(new QTabWidget)
    , m_clientView(new QTableView)
    , m_bagageView(new QTableView)
    , m_placeView(new QTableView)
    , m_aeroportView(new QTableView)
    , m_volView(new QTableView)
    , m_compagnieView(new QTableView)
    , m_database(QSqlDatabase::addDatabase("QOCI"))
{

    // CONNECT TO DATABASE -----------------------------------------------------
    QFile optFile("connection.config");
    if(optFile.open(QIODevice::ReadOnly)){
        m_database.setHostName(QString(optFile.readLine()).trimmed());
        m_database.setDatabaseName(QString(optFile.readLine()).trimmed());
        m_database.setUserName(QString(optFile.readLine()).trimmed());
        m_database.setPassword(QString(optFile.readLine()).trimmed());
    }else{
        QString msg("Impossible d'ouvrir le fichier de configuration : ");
        QMessageBox::warning(this,"Erreur",msg);
        QApplication::exit(1);
    }

    if(!m_database.open()){
        QString msg("Impossible de se connecter a la base de donnees");
        QMessageBox::warning(this,"Erreur",msg);
        QApplication::exit(1);
    }

    // MENUS -------------------------------------------------------------------
    setMenuBar(m_menubar);
    // building the menu bar
    m_menubar->addMenu(m_fileMenu);
    m_menubar->addMenu(m_optionsMenu);
    m_menubar->addMenu(m_helpMenu);
    // building the file menu
    m_fileMenu->addAction(m_quitAction);
    // building the options menu
    m_optionsMenu->addAction(m_dbConAction);
    // building the help menu
    m_helpMenu->addAction(m_aboutAction);

    // MODEL - VIEW ------------------------------------------------------------
    // client
    m_clientModel = new QSqlTableModel(this,m_database) ;
    m_clientModel->setTable("Client");
    m_clientModel->select();
    m_clientView->setModel(m_clientModel);
    m_clientView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    // Bagagae
    m_bagageModel = new QSqlTableModel(this,m_database) ;
    m_bagageModel->setTable("Bagage");
    m_bagageModel->select();
    m_bagageView->setModel(m_bagageModel);
    m_bagageView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    // Place
    m_placeModel = new QSqlRelationalTableModel(this,m_database) ;
    m_placeModel->setTable("Place");
    m_placeModel->setRelation(3, QSqlRelation("Classe", "num_classe", "nom_classe"));
    m_placeModel->setRelation(4, QSqlRelation("TypePlace", "num_type_place", "nom_type_place"));
    m_placeModel->select();
    m_placeView->setModel(m_placeModel);
    m_placeView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    // Aeroport
    m_aeroportModel = new QSqlTableModel(this,m_database) ;
    m_aeroportModel->setTable("Aeroport");
    m_aeroportModel->select();
    m_aeroportView->setModel(m_aeroportModel);
    m_aeroportView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    // Vol
    m_volModel = new QSqlTableModel(this,m_database) ;
    m_volModel->setTable("Vol");
    m_volModel->select();
    m_volView->setModel(m_volModel);
    m_volView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    // Compagnie
    m_compagnieModel = new QSqlTableModel(this,m_database) ;
    m_compagnieModel->setTable("Compagnie");
    m_compagnieModel->select();
    m_compagnieView->setModel(m_compagnieModel);
    m_compagnieView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    // CENTRAL WIDGET ----------------------------------------------------------
    setCentralWidget(m_tabWidget);
    m_tabWidget->addTab(m_clientView,QIcon("user.png"),"Clients");
    m_tabWidget->addTab(m_bagageView,QIcon("bag.png"),"Bgages");
    m_tabWidget->addTab(m_placeView,QIcon("seat.png"),"Places");
    m_tabWidget->addTab(m_aeroportView,QIcon("airport.png"), "Aeroports");
    m_tabWidget->addTab(m_volView,QIcon("airplaine.png"),"Vols");
    m_tabWidget->addTab(m_compagnieView,QIcon("agency.png"), "Compagnies");
    m_tabWidget->setIconSize(QSize(30,30));


    // CONNECTIONS -------------------------------------------------------------
    // file menu actions
    connect(m_quitAction, SIGNAL(triggered()), qApp, SLOT(quit()) );
    // options menu actions
    connect(m_dbConAction, SIGNAL(triggered()),
            this, SLOT(showConnectionDialog()) );
    // help menu actions
    connect(m_aboutAction, SIGNAL(triggered()), this, SLOT(showAboutDialog()) );
}

MainWindow::~MainWindow()
{
    
}

void MainWindow::showAboutDialog()
{
    QString text = "<b>Youssef Bouhjira <br> Ahmed Eloissifi</b>";
    text += "<br>Ecole Superieur de Technologie d'Essaouira";
    text += "<br>2012-2013";
    QMessageBox::about(this,"A propos",text);
}

void MainWindow::showConnectionDialog()
{
    ConnectionDialog::getOptions(this,"Parametres de connections");
}
