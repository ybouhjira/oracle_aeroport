INSERT INTO Client (cin,nom,prenom,tel,email,mot_de_passe,adresse,ville,pays) VALUES ('HA177714','ELOISSIFI','Ahmed',0658391729,
'el.oissifi.ahmed@gmail.com','passedemot','Maroc Chemaia quartier AlMassira N 13','Chemaia','Maroc');

INSERT INTO Client (cin,nom,prenom,tel,email,mot_de_passe,adresse,ville,pays) VALUES ('HA177710','ELMadani','Adam',0612345678,
'encoreadam1@hotmail.fr','yawyaw','Maroc Chemaia quartier AlMassira N 15','Chemaia','Maroc');

INSERT INTO Client (cin,nom,prenom,tel,email,mot_de_passe,adresse,ville,pays) VALUES ('MA151232','BOUHJIRA','YOUSSEF',0651234729,
'b.youssef91@gmail.com','onlyprogrammation','Maroc Essaouira Lotissement2 N 100','Essaouira','Maroc');

INSERT INTO Client (cin,nom,prenom,tel,email,mot_de_passe,adresse,ville,pays) VALUES ('HA165547','LAGHOUAL','Ayoub',06677169353,
'laghoual.ayoub@gmail.com','gohardorgohome','Maroc Taroudant quartier 123 N 54','Taroudant','Maroc');

INSERT INTO Client (cin,nom,prenom,tel,email,mot_de_passe,adresse,ville,pays) VALUES ('AB393934','ELKOUCHI','Zouhair',0651452122,
'el.kouchi.zozo@gmail.com','sahramaghribia','Maroc Taroudant quartier Alsahra N 6','Smara','Maroc');

-- AEROPORT --------------------------
---------------------------------
INSERT INTO Aeroport (num_aeroport,ville_aeroport,pays_aeroport) VALUES (1,'Marrakech','Maroc');
INSERT INTO Aeroport (num_aeroport,ville_aeroport,pays_aeroport) VALUES (2,'Casablanca','Maroc');
INSERT INTO Aeroport (num_aeroport,ville_aeroport,pays_aeroport) VALUES (3,'Rabat','Maroc');
INSERT INTO Aeroport (num_aeroport,ville_aeroport,pays_aeroport) VALUES (4,'Paris','France');
INSERT INTO Aeroport (num_aeroport,ville_aeroport,pays_aeroport) VALUES (5,'Chikago','USA');

-- CLASSE --------------------------
--------------------------------------
INSERT INTO Classe (num_classe,nom_classe) VALUES (1,'Premi�re');
INSERT INTO Classe (num_classe,nom_classe) VALUES (2,'Economique');
INSERT INTO Classe (num_classe,nom_classe) VALUES (3,'Affaire');

-- TYPEPLACE --------------------------
---------------------------------
INSERT INTO TypePlace (num_type_place,nom_type_place) VALUES (1,'Enfant');
INSERT INTO TypePlace (num_type_place,nom_type_place) VALUES (2,'Adulte');

-- COMPGNIE --------------------------
---------------------------------

INSERT INTO Compagnie(num_compagnie,nom_compagnie,email_compagnie,tel_compagnie) 
VALUES (1,'AIR AUSTRAL','airaustral@air.com','0.825.013.012');
INSERT INTO Compagnie(num_compagnie,nom_compagnie,email_compagnie,tel_compagnie) 
VALUES (2,'AVIANCA','avianca@air.com','0.825.86.98.83');
INSERT INTO Compagnie(num_compagnie,nom_compagnie,email_compagnie,tel_compagnie) 
VALUES (3,'AER LINGUs','aerlingus@air.com','08.21.23.02.67');
INSERT INTO Compagnie(num_compagnie,nom_compagnie,email_compagnie,tel_compagnie)
VALUES (4,'ARMAVIA','armavia@air.com','01.42.96.10.10');
INSERT INTO Compagnie(num_compagnie,nom_compagnie,email_compagnie,tel_compagnie) 
VALUES (5,'AIRLINAIR ','airlinair@air.com','0 825 808 228');

-- VOL --------------------------
---------------------------------
INSERT INTO Vol (num_vol,date_depart,date_arrive,num_compagnie,num_aeroport)
VALUES (
	1,
	(TO_DATE('01-01-2012 12:30','DD-MM-YYYY HH24:MI')),
	(TO_DATE('01-01-2012 20:00','DD-MM-YYYY HH24:MI')),
	1,
	1
);

INSERT INTO Vol (num_vol,date_depart,date_arrive,num_compagnie,num_aeroport)
VALUES (
	2,
	(TO_DATE('06-11-2012 13:30','DD-MM-YYYY HH24:MI')),
	(TO_DATE('07-11-2012 20:00','DD-MM-YYYY HH24:MI')),
	2,
	2
);
INSERT INTO Vol (num_vol,date_depart,date_arrive,num_compagnie,num_aeroport)
VALUES (
	3,
	(TO_DATE('11-01-2012 11:00','DD-MM-YYYY HH24:MI')),
	(TO_DATE('13-11-2012 21:00','DD-MM-YYYY HH24:MI')),
	3,
	3
);
INSERT INTO Vol (num_vol,date_depart,date_arrive,num_compagnie,num_aeroport)
VALUES (
	4,
	(TO_DATE('01-02-2012 09:30','DD-MM-YYYY HH24:MI')),
	(TO_DATE('01-01-2012 20:00','DD-MM-YYYY HH24:MI')),
	4,
	4
);
INSERT INTO Vol (num_vol,date_depart,date_arrive,num_compagnie,num_aeroport)
VALUES (
	5,
	(TO_DATE('08-04-2012 12:30','DD-MM-YYYY HH24:MI')),
	(TO_DATE('09-04-2012 06:00','DD-MM-YYYY HH24:MI')),
	5,
	5
);

-- BAGGAGE --------------------------
---------------------------------
INSERT INTO Bagage (num_bagage,cin,poids,designation) VALUES (1,'HA177714',3,'samsonite');
INSERT INTO Bagage (num_bagage,cin,poids,designation) VALUES (2,'HA177714',70,' valise');
INSERT INTO Bagage (num_bagage,cin,poids,designation) VALUES (3,'MA151232',100,' grande valise');
INSERT INTO Bagage (num_bagage,cin,poids,designation) VALUES (4,'HA165547',60,'grand sac');
INSERT INTO Bagage (num_bagage,cin,poids,designation) VALUES (5,'AB393934',1.3,'sac � dos');


-- PLACE --------------------------
--------------------------------------
INSERT INTO Place (num_place,num_vol,cin,num_classe,num_type_place,prix) VALUES (1,1,'HA177714',1,1,5000);
INSERT INTO Place (num_place,num_vol,cin,num_classe,num_type_place,prix) VALUES (2,2,'HA177710',2,2,3000);
INSERT INTO Place (num_place,num_vol,cin,num_classe,num_type_place,prix) VALUES (3,3,'MA151232',3,1,6000);
INSERT INTO Place (num_place,num_vol,cin,num_classe,num_type_place,prix) VALUES (4,4,'HA165547',1,2,8000);
INSERT INTO Place (num_place,num_vol,cin,num_classe,num_type_place,prix) VALUES (5,5,'AB393934',2,2,1500);

