-- DROP CONSTRAINTS
ALTER TABLE Place DROP CONSTRAINT place_num_vol_fk;
ALTER TABLE Place DROP CONSTRAINT place_num_classe_fk;
ALTER TABLE Place DROP CONSTRAINT place_num_type_place_fk;
ALTER TABLE Bagage DROP CONSTRAINT bagage_cin_fk;
ALTER TABLE Vol DROP CONSTRAINT vol_num_compagnie_fk;
ALTER TABLE Vol DROP CONSTRAINT vol_num_aeroport_fk;

-- CLEAN TABLES
DELETE FROM Client;
DELETE FROM Place;
DELETE FROM Classe;
DELETE FROM TypePlace ;
DELETE FROM Bagage ;
DELETE FROM Vol;
DELETE FROM Compagnie;
DELETE FROM Aeroport;

-- Put constraints again
ALTER TABLE Place
ADD CONSTRAINT place_num_vol_fk
FOREIGN KEY (num_vol)
REFERENCES Vol(num_vol);

ALTER TABLE Place
ADD CONSTRAINT place_num_classe_fk
FOREIGN KEY (num_classe)
REFERENCES Classe(num_classe);

ALTER TABLE Place
ADD CONSTRAINT place_num_type_place_fk
FOREIGN KEY (num_type_place)
REFERENCES TypePlace(num_type_place);

ALTER TABLE Bagage
ADD CONSTRAINT bagage_cin_fk
FOREIGN KEY (cin)
REFERENCES Client(cin);

ALTER TABLE Vol
ADD CONSTRAINT vol_num_compagnie_fk
FOREIGN KEY (num_compagnie)
REFERENCES Compagnie(num_compagnie);

ALTER TABLE Vol
ADD CONSTRAINT vol_num_aeroport_fk
FOREIGN KEY (num_aeroport)
REFERENCES Aeroport(num_aeroport);